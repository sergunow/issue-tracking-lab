// issue tracking lab.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"
#include <iostream>
#include <string>
#include <math.h>

using namespace std;

const int lengthTime = 6;

struct Task {
	int number;
	string time;
};

double GetTimeForTask(Task inputTask);
bool IsValidCount(int numTasks);
bool IsValidTime(string time);
double CalculateCountOfWeeks(Task*inputTask, int numTasks);
bool IsMoreThatDay(Task inputTask);


int main()
{
	int numTasks = 0;
	setlocale(0, "");
	cout << "������� ���������� �����: " << endl;
	cin >> numTasks;
	while (IsValidCount(numTasks) != true)
	{
		cout << "�� ����� ������������ ��������! ������� ���������� ����� �����: " << endl;
		cin >> numTasks;
	}
	Task *inputTasks = new Task[numTasks];
	memset(inputTasks, 0, sizeof(int));
	cout << endl;
	for (int i = 0; i < numTasks; i++)
	{
		inputTasks[i].number = i + 1;
		cout << "������� �����, ����������� ��� ���������� ������ �" << inputTasks[i].number << ": " << endl;

		cin >> inputTasks[i].time;
		while (IsValidTime(inputTasks[i].time) == false)
		{
			cout << "�� ����� ������������ ��������! ������� ����������� ��� ���������� ������ �" << inputTasks[i].number << ", �����: " << endl;
			cin >> inputTasks[i].time;
		}
	}
	cout << endl;
	cout << "��� ���������� ���� ����� ����������� " << CalculateCountOfWeeks(inputTasks, numTasks) << " 40 ������� ������" << endl;
	for (int i = 0; i < numTasks; i++)
	{
		if (IsMoreThatDay(inputTasks[i]))
		{
			cout.precision(4);
			cout << "��� ���������� ������ �" << inputTasks[i].number << " ����������� ������ 8 �����" << " � ����������������� ����� " << GetTimeForTask(inputTasks[i]) << "�����" << endl;
		}
	}
	system("pause");


	return 0;
}

double GetTimeForTask(Task inputTask)
{
	double time, frac, integer;
	inputTask.time[2] = ',';
	frac = modf(stod(inputTask.time), &integer);
	time = integer + (frac * 100) / 60;
	return time;
}

bool IsValidCount(int numTasks)
{
	if ((numTasks > 0) && (numTasks <= 1000))
	{
		return true;
	}
	return false;
}

bool IsValidTime(string time)
{
	if (time.length() == 5 && time[2] == ':' && time[0] >= '0' && time[0] <= '9' && time[1] >= '0' && time[1] <= '9'
		&& time[3] >= '0' && time[3] <= '9' && time[4] >= '0' && time[4] <= '9')
	{
		return true;
	}
	return false;
}

double CalculateCountOfWeeks(Task*inputTask, int numTasks)
{
	double numWeeks = 0;
	for (int i = 0; i < numTasks; i++)
	{
		numWeeks += GetTimeForTask(inputTask[i]);
	}
	numWeeks = ceil(numWeeks / 40);
	return numWeeks;
}

bool IsMoreThatDay(Task inputTask)
{
	if (GetTimeForTask(inputTask) > 8)
	{
		return true;
	}
	return false;
}